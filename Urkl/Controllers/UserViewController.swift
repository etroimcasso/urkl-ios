//
//  UserViewController.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/21/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    // MARK: Properties
    var currentUser: User? = nil {
        didSet {
            self.adjustInterfaceForUser()
        }
    }
    private let signInText = "Sign In"
    private let signOutText = "Sign Out"

    // MARK: Outlets
    @IBOutlet weak var signInOutButton: UIBarButtonItem!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!

    // MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        currentUser = getCurrentUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func unwindToUserProfile(sender: UIStoryboardSegue) {
        adjustInterfaceForUser()
    }

    // MARK: Actions
    @IBAction func signInOutItemTapped(_ sender: UIBarButtonItem) {
        if currentUser == nil {
            performSegue(withIdentifier: "goToSignIn", sender: nil)
        } else {
            UserAuthManager.shared.signOutUser()
            self.currentUser = nil
        }
    }

    // MARK: Private Methods
    private func getCurrentUser() -> User? {
        if UserAuthManager.shared.currentUser != nil, let user = UserAuthManager.shared.currentUser {
            return user
        } else {
            return nil
        }
    }

    private func adjustInterfaceForUser() {
        //Called every time the user property changes

        let userSignedIn = currentUser != nil

        // Hide the Create Account button if a user is signed in
        createAccountButton.isHidden = userSignedIn

        // Set the Sign In / Out button's text
        signInOutButton.title = (userSignedIn) ? signOutText : signInText

        //Set interface labels
        usernameLabel.text = currentUser?.username
    }

}
