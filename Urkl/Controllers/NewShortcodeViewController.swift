//
//  NewShortcodeViewController.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON
import os.log

class NewShortcodeViewController: UIViewController {
    // MARK: Properties
    var urlTextFieldDelegate: UITextFieldDelegate!
    var titleTextFieldDelegate: UITextFieldDelegate!
    var shortcode: Shortcode?

    // MARK: Outlets
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var shortcodeField: UITextField!
    @IBOutlet weak var tagsField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var detectInfoButton: UIButton!
    @IBOutlet weak var publicToggleStack: UIStackView!
    @IBOutlet weak var privacyToggleSwitch: UISwitch!

    // MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the

        //Get random shortcode
        getNewShortcode()

        //Disable add button
        addButton.isEnabled = false
        //Disable Detect button
        detectInfoButton.isEnabled = false

        //Set UITextFieldDelegates
        urlTextFieldDelegate = UrlTextFieldDelegate()
        urlField.delegate = urlTextFieldDelegate
        titleTextFieldDelegate = TitleTextFieldDelegate()
        titleField.delegate = titleTextFieldDelegate

        // Make changes to urlField and titleField call updateButtonsEnabledStates()
        urlField.addTarget(self, action: #selector(updateButtonsEnabledStates(_:)), for: .editingChanged)
        titleField.addTarget(self, action: #selector(updateButtonsEnabledStates(_:)), for: .editingChanged)

        // Hide public toggle if there is no user
        publicToggleStack.isHidden = UserAuthManager.shared.currentUser == nil

        //Add borders to descriptionField, make them gray, and round the corners
        descriptionField.layer.borderWidth = 0.2
        descriptionField.layer.borderColor = UIColor.gray.cgColor
        descriptionField.layer.cornerRadius = 5

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button == addButton else {
            os_log("The Add button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        let shortcode = Shortcode(
            shortcode: shortcodeField.text!,
            url: urlField.text!,
            title: titleField.text!,
            description: descriptionField.text!,
            isPublic: !privacyToggleSwitch.isOn,
            user: UserAuthManager.shared.currentUser?.username ?? "0",
            rating: 0,
            totalRatings: 0,
            views: 0,
            createdAt: Date(),
            tags: tagsField.text!)
        self.shortcode = shortcode
        if shortcode != nil {
            addShortcodeToDatabase(shortcode: shortcode!)
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: Actions
    @IBAction func refreshShortcodeButtonDidTap(_ sender: UIButton) {
        getNewShortcode()
    }

    @IBAction func detectInfoButtonDidTap(_ sender: UIButton) {
        let urlString = InputValidator.shared.normalizeUrl(url: urlField.text!)
        UrklLangAPIManager.shared.getTitle(
            url: urlString,
            onSuccess: { title in
                self.titleField.text = title
                self.updateButtonsEnabledStates(self)
        },
            onFailure: { _ in
        })

        UrklLangAPIManager.shared.getArticleTags (
            url: urlString,
            onSuccess: { tags in
                self.tagsField.text = tags
        },
            onFailure: { _ in
        })

        UrklLangAPIManager.shared.getLinkDescription(
            url: urlString,
            onSuccess: { desc in
                var finalDesc = desc
                let trimmedSuffix = UrklConfigurationManager.shared.trimmedTextSuffix
                let maxDesc = UrklConfigurationManager.shared.maximumDescriptionLength
                if desc.count > maxDesc {
                    let index = desc.index(desc.startIndex,
                                           offsetBy: (maxDesc - trimmedSuffix.count))
                    let trimmedDesc = "\(desc[..<index])"
                    finalDesc = trimmedDesc + trimmedSuffix
                }
                self.descriptionField.text = finalDesc
        }, onFailure: { _ in })
    }

    @objc func updateButtonsEnabledStates(_ sender: AnyObject) {
        //print("Text Field Changed Sender: \(sender)")

        /*
         Enable detect information button if urlField.text is valid URL
         */
        guard let urlText = urlField.text as NSString? else {
            fatalError("URL is not a string")
        }

        let urlTextIsValid = InputValidator.shared.validateUrl(urlString: urlText)
        let titleIsValid = !(titleField.text?.isEmpty)!
        let shortcodeIsValid = !(shortcodeField.text?.isEmpty)!

        detectInfoButton.isEnabled = urlTextIsValid

        /* Only enable add button if:
         *URL is valid
         *Title is not empty
         *Shortcode is not empty
         */
        addButton.isEnabled = urlTextIsValid && titleIsValid && shortcodeIsValid

    }

    // MARK: Private Methods
    private func getNewShortcode() {
        UrklShortcodeAPIManager.shared.getnewShortcode(onSuccess: { json in
            DispatchQueue.main.async {
                self.shortcodeField.text = json["response"].stringValue
            }

        }, onFailure: { error in
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.show(alert, sender: nil)
        })
    }

    private func addShortcodeToDatabase(shortcode: Shortcode) {
        UrklShortcodeAPIManager.shared.postNewShortcode(
            url: shortcode.url,
            shortcode: shortcode.shortcode,
            title: shortcode.title,
            description: shortcode.description ?? "" ,
            isPublic: shortcode.isPublic,
            tags: shortcode.tags,
            onSuccess: { _ in

        }, onFailure: { _ in

        })
    }

}
