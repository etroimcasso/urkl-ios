//
//  ViewController.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON

class ShortcodeTableViewController: UITableViewController {
    // MARK: Outlets

    // MARK: Properties
    var shortcodes = [Shortcode]()
    var rawShortcodeData: JSON?
    let cellHeight: CGFloat = 188.0
    var sortBy: ShortcodeArrayFilterSorter.SortType = .date
    var sortDirection: ShortcodeArrayFilterSorter.SortDirection = .descend

    override func viewDidLoad() {
        super.viewDidLoad()

        // Don't load the shortcode list until user data is retrieved
        UserAuthManager.shared.initialize(completion: {
            self.retrieveShortcodeListFromRemote(onComplete: {
                self.populateTableWithShortcodeData(onComplete: {
                })
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shortcodes.count
    }

    /**
     Sets the cell height to 0 (hidden) unless
     The item is public or
     the item is private and the current user is the owner or
     the item is private and the current user is the administrator and security
        settings are set to allow the administrator omnipotence
     */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        guard shortcodes.count > 0 else {
            print("0 count error")
            return 0
        }
        let shortcode = shortcodes[indexPath.row]

        var display: Bool = true

        //If the shortcode isn't public and the current user isn't the shortcode's user,
        //  set display = false
        if !shortcode.isPublic &&
            (UserAuthManager.shared.currentUser?
.username ?? "") != shortcode.user {
            display = false
        }

        if !display {
            return 0
        } else {
            return self.cellHeight
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("Cell added / removed")
        let cellIdentifier = "ShortcodeTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? ShortcodeTableViewCell else {
            fatalError("The dequeued cell is not an instance of ShortcodeTableViewCell")
        }

        let shortcode = shortcodes[indexPath.row]

        // Configure the cell...
        cell.shortcode = shortcode.shortcode
        cell.url = shortcode.url
        cell.urlLabel.text = shortcode.url
        cell.title = shortcode.title
        cell.titleButton.setTitle(shortcode.title, for: .normal)
        cell.descriptionField.text = shortcode.description ?? ""
        //print("Description: \(shortcode.description ?? "")")
        cell.isPublic = shortcode.isPublic
        //cell.isPublicToggle toggle it true or false according to shortcode.isPublic
        cell.userLabel.text = "User: \((Int(shortcode.user) != 0) ? shortcode.user : "Nobody" )"
        //Cell rating stuff
        //cell total ratings stuff
        cell.viewsLabel.text = "Views: \(String(shortcode.views))"
        //cell created_at thing
        //Cell tags list

        //Set cell color
        cell.setCellColors(isPrivate: !shortcode.isPublic)

        return cell
    }

    // MARK: Data Loading Methods

    /**
     Retrieves the JSON list of all shortcodes from the remote server
    */
    private func retrieveShortcodeListFromRemote(onComplete: @escaping(()) -> Void) {
        UrklShortcodeAPIManager.shared.getAllShortcodes(onSuccess: { json in
            DispatchQueue.main.async {
                self.rawShortcodeData = json
                onComplete(())
            }
        }, onFailure: { error in
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            self.show(alert, sender: nil)
        })

    }

    /**
     Parses the contents of rawShortcodeData into its properties
    */
    private func populateTableWithShortcodeData(onComplete: @escaping(()) -> Void) {
        var newShortcodes = [Shortcode]()
        for item in self.rawShortcodeData!["response"].arrayValue {
            let shortcode = item["shortcode"].stringValue
            let url = item["url"].stringValue
            let title = item["page_title"].stringValue
            let desc = item["desc"].stringValue
            let user = item["user"].stringValue
            let isPublic = item["is_public"].boolValue
            let rating = item["rating"].intValue
            let totalRatings = item["totalRatings"].intValue
            let views = item["views"].intValue
            let createdAt = item["created_at"].doubleValue
            let tags = item["tags"].stringValue

            guard let newShortcode = Shortcode(
                shortcode: shortcode,
                url: url,
                title: title,
                description: desc,
                isPublic: isPublic,
                user: user,
                rating: rating,
                totalRatings: totalRatings,
                views: views,
                createdAt: Date(timeIntervalSince1970: createdAt),
                tags: tags) else {
                    fatalError("Cannot instantiate shortcode")
            }
            newShortcodes.append(newShortcode)
            //addShortcodeToTable(shortcode: newShortcode)
        }
        self.shortcodes = newShortcodes
        self.filterSortAndRefreshTable()
    onComplete(())
    }

    /**
        Clears the Shortcodes list
    */
    private func clearShortcodesList() {
        self.shortcodes.removeAll()
    }

    // MARK: Navigation
    @IBAction func unwindToShortcodeList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? NewShortcodeViewController {
            let shortcode = sourceViewController.shortcode!

            // Add a new shortcode

            shortcodes.append(shortcode)
            self.retrieveShortcodeListFromRemote(onComplete: { _ in
                self.populateTableWithShortcodeData(onComplete: { _ in
                })
            })

            /*
            self.populateTableWithShortcodeData(onComplete: { _ in
                print("reloading now")
                self.tableView.reloadData()
            })
            //self.tableView.reloadData()
            */
            //addShortcodeToTable(shortcode: shortcode)
        }
    }

    // MARK: Actions
    /**
     Refreshes the table view
    */
    @IBAction func refreshTableView(_ sender: UIRefreshControl) {
        clearShortcodesList()
        let refreshControl: UIRefreshControl = sender
        self.retrieveShortcodeListFromRemote(onComplete: { _ in
            self.populateTableWithShortcodeData(onComplete: {
                refreshControl.endRefreshing()
            })
        })
    }

    // MARK: Private Methods
    private func addShortcodeToTable(shortcode: Shortcode) {
        let newIndexPath = IndexPath(row: self.shortcodes.count, section: 0)
        self.shortcodes.append(shortcode)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    /**
     Applies filters, sorting, and reloads table view
     
    */
    private func filterSortAndRefreshTable() {
        var alteredArray = shortcodes
        alteredArray = applyFilters(array: shortcodes)
        alteredArray = applySorting(array: alteredArray)
        shortcodes = alteredArray
        self.tableView.reloadData()
    }
    /**
     Iterates through filters array and outputs a filtered User array
    */
    private func applyFilters(array: [Shortcode]) -> [Shortcode] {
        return array
    }
    /**
        Applies sorting method and outputs result
     DOES NOT TOUCH SHORTCODES ARRAY
    */
    private func applySorting(array: [Shortcode]) -> [Shortcode] {
        return ShortcodeArrayFilterSorter.shared.sortUserArrayWith(
            array: array,
            sortBy: sortBy,
            direction: sortDirection)
    }

}
