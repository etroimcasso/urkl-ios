//
//  LoginUserViewController.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/23/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class LoginUserViewController: UIViewController {
    // MARK: Properties
    var currentUser: User?

    // MARK: Outlets
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // Disable sign-in button
        updateSignInButtonStatus(self)

        // Add editChange event listeners to usernameField and passwordField
        usernameField.addTarget(self, action: #selector(updateSignInButtonStatus(_:)), for: .editingChanged)
        passwordField.addTarget(self, action: #selector(updateSignInButtonStatus(_:)), for: .editingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        let userProfileView = segue.destination as? UserViewController

        userProfileView?.currentUser = self.currentUser
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: Actions
    @IBAction func signInDidTap(_ sender: UIBarButtonItem) {
        UserAuthManager.shared.authenticateUser(
            username: usernameField.text!,
            password: passwordField.text!,
            onSuccess: { user in
                if user != nil {
                    self.currentUser = user
                    self.performSegue(withIdentifier: "unwindToUserProfile", sender: self)
                } else {
                    print("Incorrect Username or Password")
                }
        }, onFailure: { _ in
            self.currentUser = nil
        })

    }

    // MARK: Methods
    /**
     Enables the sign-in button if both Username and Password text is valid
    */

    @objc func updateSignInButtonStatus(_ sender: AnyObject) {
        let isUsernameValid = InputValidator.shared.validateUsername(username: usernameField.text!)
        let isPasswordValid = InputValidator.shared.validatePassword(password: passwordField.text!)
        signInButton.isEnabled = isUsernameValid && isPasswordValid
    }

    // MARK: Private Methods
    /*
    func displayIncorrectCredentialsAlert() {
        let defaultAction = UIAlertAction(
            title: "Ok",
            style: .default) { (action) in
                
        }
    }
    */
}
