//
//  ConfigurationService.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON

// NO TRAILING SLASHES

class UrklConfigurationManager: NSObject {
    // MARK: Shared Instance Initializer
    static let shared = UrklConfigurationManager()

    // MARK: Configuration Values
    let baseUrl: String = "https://urkl.ws"
    let urklApiBaseUrl: String = "https://urkl.ws/api"
    let paramsUrl: String = "https://urkl.ws/api/p"

    // MARK: Runtime Configuration Values
    var maximumTitleLength: Int = 0
    var maximumDescriptionLength: Int = 0
    var minimumUsernameLength: Int = 0
    var maximumUsernameLength: Int = 0
    var minimumPasswordLength: Int = 0
    var maximumPasswordLength: Int = 0
    var trimmedTextSuffix: String = ""

    // MARK: Parameters endpoints & URL
    static let getMaximumTitleLengthEndpoint: String = "/max_title_length"
    static let getMinimumUsernameLengthEndpoint: String = "/min_username_length"
    static let getMaximumUsernameLengthEndpoint: String = "/max_username_length"
    static let getMinimumPasswordLengthEndpoint: String = "/min_password_length"
    static let getMaximumPasswordLengthEndpoint: String = "/max_password_length"
    static let getTrimmedTextSuffixEndpoint: String = "/trimmed_text_suffix"
    static let getUrklConfigurationEndpoint: String = "/all"

    // MARK: Init
    override init() {
        super.init()
        getUrklConfiguration(onSuccess: { json in
            self.maximumTitleLength = json["max_title_length"].intValue
            self.maximumDescriptionLength = json["max_desc_length"].intValue
            self.minimumUsernameLength = json["min_username_length"].intValue
            self.maximumUsernameLength = json["max_username_length"].intValue
            self.minimumPasswordLength = json["min_password_length"].intValue
            self.maximumPasswordLength = json["max_password_length"].intValue
            self.trimmedTextSuffix = json["trimmed_text_suffix"].stringValue
        }, onFailure: { error in
            fatalError(error.localizedDescription)
        })
        return
    }

    // MARK: API Requests
    /**
    Retrieves Urkl configuration details from API
    */
    private func getUrklConfiguration(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.getDataFrom(
            url: self.paramsUrl + UrklConfigurationManager.getUrklConfigurationEndpoint,
            onSuccess: { json in
                onSuccess(json)
        }, onFailure: { error in
            onFailure(error)
        })
    }
}
