//
//  InputValidator.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/20/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class InputValidator: NSObject {
    // MARK: Shared Instance Initializer
    static let shared = InputValidator()

    // MARK: Public Methods
    /**
     Validates URL
    */
    func validateUrl (urlString: NSString) -> Bool {
        let regEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", regEx).evaluate(with: urlString)
    }

    func normalizeUrl (url: String ) -> String {
        //If URL does not have protocol indicator, add http://
        var finalUrl = url
        if !url.hasPrefix("http://") && !url.hasPrefix("https://") {
            finalUrl = "http://\(url)"
        }
        return finalUrl as String
    }

    func validateEmail(emailString: NSString) -> Bool {
        return true
    }
    /**
     Returns true if the username is between minimum and maximum allowed lengths
    */
    func validateUsername(username: String) -> Bool {
        return !username.isEmpty &&
            username.count >= UrklConfigurationManager.shared.minimumUsernameLength &&
            username.count <= UrklConfigurationManager.shared.maximumUsernameLength
    }

    /**
     Returns true if the password is within the allowed length range
    */
    func validatePassword(password: String) -> Bool {
        return !password.isEmpty &&
            password.count >= UrklConfigurationManager.shared.minimumPasswordLength &&
            password.count <= UrklConfigurationManager.shared.maximumPasswordLength
    }
}
