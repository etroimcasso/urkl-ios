//
//  RESTfulRequestor.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/19/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

//AKA SLEEPY

/**
 Simple singleton object with one purpose: Send and receive RESTful
 data
*/
class RESTfulRequestor: NSObject {

    // MARK: Shared Instance Initializer
    static let shared = RESTfulRequestor()

    // MARK: Requestor Function
    func getDataFrom(url: String, onSuccess: @escaping(JSON) -> Void,
                     onFailure: @escaping(Error) -> Void) {
        Alamofire.request(url, method: .get).responseString(completionHandler: { response in
            if response.error == nil, let responseString = response.result.value {
                onSuccess(JSON.init(parseJSON: responseString))
            } else {
                onFailure(response.error!)
            }
        })
    }

    // MARK: Post Function
    func postDataTo(url: String, data: Parameters, onSuccess: @escaping(JSON) -> Void,
                    onFailure: @escaping(Error) -> Void) {
        Alamofire.request(url, method: .post, parameters: data).responseString(completionHandler: { response in
            if response.error == nil, let responseString = response.result.value {
                onSuccess(JSON.init(parseJSON: responseString))
            } else {
                onFailure(response.error!)
            }
        })
    }

}
