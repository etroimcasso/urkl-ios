//
//  LangAPIManager.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON

class UrklLangAPIManager: NSObject {
    // MARK: Shared Instance Initializer
    static let shared = UrklLangAPIManager()

    // MARK: Properties
    let baseURL = UrklConfigurationManager.shared.urklApiBaseUrl

    // MARK: API Endpoints
    static let getTitleEndpoint = "/m/title_detect"
    static let getLinkDescriptionEndpoint = "/m/getLinkDescription"
    static let getArticleTagsEndpoint = "/m/detectArticleTags"

    // MARK: API Methods

    /**
     Detect title at URL
    */
    func getTitle(url: String, onSuccess: @escaping(String) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UrklLangAPIManager.getTitleEndpoint,
            data: [
                "url": url
            ],
            onSuccess: { json in
                onSuccess(json["response"].stringValue)

            }, onFailure: { error in
                onFailure(error)
        })
    }

    func getArticleTags(url: String, onSuccess: @escaping(String) -> Void, onFailure: @escaping(Error) -> Void) {
        
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UrklLangAPIManager.getArticleTagsEndpoint,
            data: [
                "url": url
            ],
            onSuccess: { json in
                print("Tags: \(json)")
                var tagString: String = ""
                for (index, tag) in json["response"] {
                    tagString += (Int(index) == 0) ? "\(tag)":", \(tag)"
                }
                onSuccess(tagString)

        }, onFailure: { error in
            onFailure(error)
        })
    }

    func getLinkDescription(
        url: String,
        onSuccess: @escaping(String) -> Void,
        onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UrklLangAPIManager.getLinkDescriptionEndpoint,
            data: [
                "url": url
            ],
            onSuccess: { json in
                onSuccess(json["response"].stringValue)
        },
            onFailure: { error in
                onFailure(error)
        })
    }

}
