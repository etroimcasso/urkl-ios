//
//  UserAuthManager.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/19/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
 The purpose of this singleton class is to maintain a consistent User session across all
 components and views.
 
 It is responsible for handling user signin and signout, and retaining user information
 between application sessions. These user sessions persist on the device, so
 the app can be closed and the user will remain logged in the next time they
 load the app.
 */
class UserAuthManager: NSObject, NSCoding {
    // MARK: Shared Instance Initializer
    static let shared = UserAuthManager()

    // MARK: Properties
    var currentUser: User?

    // MARK: Types
    struct PropertyKey {
        static let username = "username"
    }

    // MARK: API URL
    let baseURL = "https://urkl.ws/api/u"

    // MARK: User API Endpoints
    static let getUserInfoEndpoint = "/info"
    static let createNewUserEndpoint = "/new"
    static let getNumberOfUserAccountsEndpoint = "/count"
    static let loginUserEndpoint = "/login"
    static let getUserSessionDataEndpoint = "/getSessionUserData"
    static let doesUserExistEndpoint = "/doesUserExist"
    static let getUserDataEndpoint = "/getUserData"

    // MARK: Init
    override init() {
        super.init()

    }

    // MARK: NSCoding Methods

    func encode(with aCoder: NSCoder) {
        aCoder.encode(currentUser?.username, forKey: PropertyKey.username)
    }

    required init?(coder aDecoder: NSCoder) {

    }

    // MARK: Methods
    /**
     Performs init tasks for object. Is separate from init so  execution may
     be halted until user information is retreived by tying execution into
     the completion handler of this function.
     
    */
    func initialize(completion: @escaping(()) -> Void) {
        //Initialize object data (figure out which user is logged in (or not))
        determineCurrentUser(completion: {
            print("determined current user: \(self.currentUser?.username ?? "No user")")
            completion(())
        })
    }

    /**
     Signs in user.
     onSuccess returns a User object if successful, nil if not
    */
    func authenticateUser(
        username: String,
        password: String,
        //vv returns true or false depending on if the user was successfully authenticated
        onSuccess: @escaping(User?) -> Void,
        onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UserAuthManager.loginUserEndpoint,
            data: [
                "username": username,
                "password": password
            ],
            onSuccess: { json in
                if json["response"].boolValue {
                    self.getUserObject(
                        username: username,
                        onSuccess: { user in
                            if user != nil {
                                self.currentUser = User(
                                    username: (user?.username)!,
                                    email: (user?.email)!,
                                    joinDate: (user?.joinDate)!,
                                    lastVisited: (user?.lastVisited)!,
                                    userIsAdmin: (user?.isAdmin)!,
                                    avatarId: user?.avatarId)
                                onSuccess(self.currentUser!)
                            } else {
                                onSuccess(nil)
                            }
                    }, onFailure: { _ in
                        self.currentUser = nil
                    })
                } else {
                    onSuccess(nil)
                }
        }, onFailure: { error in
            onFailure(error)
        })
    }

    func signOutUser() {
        self.currentUser = nil

        // Remove user from persistence thingy
        // Destroy session on server (If needed)
    }

    func getUserObject(
        username: String,
        onSuccess: @escaping(User?) -> Void,
        onFailure: @escaping(Error) -> Void) {
        // Make request to getUserDataEndpoint
        // Handle that ["response"] may be nil / empty
        // If empty, return nil
        // If not empty, construct a User object from the results,
        //    and return that object with onSuccess()
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UserAuthManager.getUserDataEndpoint,
            data: [
                "username": username
            ],
            onSuccess: { user in
                if user == JSON.null {
                    onSuccess(nil)
                } else {
                    let nUser: JSON = user["response"]
                    let name: String = nUser["username"].stringValue
                    let email: String = nUser["email"].stringValue
                    let joinDate: Date = Date(timeIntervalSince1970: nUser["join_date"].doubleValue)
                    let lastVisited: Date = Date(timeIntervalSince1970: nUser["last_visited"].doubleValue)
                    let userIsAdmin: Bool = nUser["admin"].boolValue
                    //let avatarId: String? = nUser["]
                    onSuccess(User(
                        username: name,
                        email: email,
                        joinDate: joinDate,
                        lastVisited: lastVisited,
                        userIsAdmin: userIsAdmin,
                        avatarId: ""))
                }

        }, onFailure: { error in
            onFailure(error)
        })
    }

    // MARK: Private Methods
    /**
     Retrieve user from persistent storage
    */

    /**
     Store user in persistent storage
    */

    /**
     Check that user exists
    */
    private func doesUserExist(
        username: String,
        onSuccess: @escaping(Bool) -> Void,
        onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UserAuthManager.doesUserExistEndpoint,
            data: [
                "user": username
            ],
            onSuccess: { json in
                onSuccess(json["response"].boolValue)
        }, onFailure: { error in
            onFailure(error)
        })
    }

    /**
     Run initialization logic for beginning of app run
     - returns:
        - completion: Callback function that returns True if no errors, false if errors
    */
    private func determineCurrentUser(completion: @escaping(()) -> Void) {
        /*
        *Checks persistent storage for username
            *Username? - check that username exists
        *exists?: Retrieve user object from server, store in currentUser
        *doesn't exist?: Remove username from storage, set currentUser to nil
        *No username? - set currentUser to nil
         */
        completion(())
    }

    /**
     Write user object to persistent storage
    */
    private func storeUser(user: User) {

    }

    /**
     Remove user object from persistent storage
    */
    private func unstoreUser() {

    }

    private func getUserFromStore() -> User? {
        return nil
    }

}
