//
//  ArrayFilterSorter.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/24/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

/**
 Performs sorting and filtering operations on arrays of User types
 
 •Basically, ShortcodeListViewController has two arrays for sortTypes and filterTypes
 •These arrays are iterated through, and relevant UserArrayFilterSorter methods are called
    with these sortTypes and filterTypes to produce a sorted and filtered array
    for the tableView to display
 
 This class ONLY takes in an input array and a filter / sort type, and returns
    an array of User objects. That is ALL this class will do. Treat it like
    a functional program - no side effects, only inputs with consitent output
 */
class ShortcodeArrayFilterSorter: NSObject {
    // MARK: Shared instance
    static let shared = ShortcodeArrayFilterSorter()

    // MARK: Enumerators
    enum SortDirection {
        case ascend
        case descend
    }
    enum SortType {
        case date
        case title
        case user
        case views
    }

    enum FilterType {
        case showPostsForUser
        case onlyPublic
    }
    // MARK: Methods

    /**
     Takes a SortType and a SortDirection and returns a sorted array
    */
    func sortUserArrayWith(array: [Shortcode], sortBy: SortType, direction: SortDirection) -> [Shortcode] {
        return array
    }
    /**
     Takes a filter type and an argument for that filter type, and returns
     a filtered User array
    */
    func filterUserArrayWith(array: [Shortcode], filter: FilterType, argument: [String]) -> [Shortcode] {
        return array
    }

    // MARK: Private Methods

    private func sortArrayByDateAscending(array: [Shortcode]) -> [Shortcode] {
        return array
    }

    private func sortArrayByDateDescending(array: [Shortcode]) -> [Shortcode] {
        return array
    }

}
