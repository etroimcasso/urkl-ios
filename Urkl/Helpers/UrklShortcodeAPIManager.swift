//
//  APIManager.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit
import SwiftyJSON

class UrklShortcodeAPIManager: NSObject {
    // MARK: Shared Instance Initializer
    static let shared = UrklShortcodeAPIManager()
    // MARK: API URL
    let baseURL: String = "\(UrklConfigurationManager.shared.urklApiBaseUrl)/sc"

    // MARK: Shortcode API Endpoints
    static let getAllPostsEndpoint = "/lists/all"
    static let getNewShortcodeEndpoint = "/gen"
    static let getPossibleNumberOfShortcodesEndpoint = "/possible"
    static let getShortcodesLengthEndpoint = "/length"
    static let getRemainingNumberOfShortcodesEndpoint = "/remain"
    static let getNumberOfUsedShortcodesEndpoint = "/count"
    static let postNewShortcodeEndpoint = "/new"
    static let updateShortcodePrivacySettingEndpoint = "/updatePrivacySetting"

    // MARK: GET
    /**
    Get all Shortcodes in database
    */
    func getAllShortcodes(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.getDataFrom(
            url: self.baseURL + UrklShortcodeAPIManager.getAllPostsEndpoint,
            onSuccess: { json in
                onSuccess(json)
        }, onFailure: { error in
            onFailure(error)
        })

    }

    /**
    Get a new random shortcode
    */
    func getnewShortcode(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.getDataFrom(
            url: self.baseURL + UrklShortcodeAPIManager.getNewShortcodeEndpoint,
            onSuccess: { json in
                onSuccess(json)
        }, onFailure: { error in
            onFailure(error)
        })
    }

    /**
     Get the possible number of shortcodes
     */
    func getPossibleNumberOfShortcodes(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.getDataFrom(
            url: self.baseURL + UrklShortcodeAPIManager.getPossibleNumberOfShortcodesEndpoint,
            onSuccess: { json in
                onSuccess(json)
        }, onFailure: { error in
            onFailure(error)
        })
    }

    /**
     Get the allowed shortcode length
     */
    func getAllowedShortcodeLength(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        RESTfulRequestor.shared.getDataFrom(
            url: self.baseURL + UrklShortcodeAPIManager.getShortcodesLengthEndpoint,
            onSuccess: { json in
                onSuccess(json)
        }, onFailure: { error in
            onFailure(error)
        })

    }

    // MARK: POST

    /**
     Submit new shortcode to server
    */
    func postNewShortcode(
        url: String,
        shortcode: String,
        title: String,
        description: String,
        isPublic: Bool,
        tags: String,
        onSuccess: @escaping(Bool) -> Void,
        onFailure: @escaping(Error) -> Void) {
        let user = UserAuthManager.shared.currentUser?.username ?? String(0) //Server uses 0 for no user value
        RESTfulRequestor.shared.postDataTo(
            url: self.baseURL + UrklShortcodeAPIManager.postNewShortcodeEndpoint ,
            data: [
                "url": url,
                "shortcode": shortcode,
                "title": title,
                "desc": description,
                "is_public": isPublic,
                "image_id": shortcode,
                "tags": tags,
                "user": user
            ],
            onSuccess: { json in
                print(json)
        }, onFailure: { error in
            onFailure(error)
        })
    }

}
