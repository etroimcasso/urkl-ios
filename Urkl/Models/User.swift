//
//  User.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/21/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class User {
    // MARK: Properties
    var username: String
    var email: String
    var joinDate: Date
    var lastVisited: Date
    var isAdmin: Bool!
    var avatarId: String?

    // MARK: Initialization
    init?(
        username: String,
        email: String,
        joinDate: Date,
        lastVisited: Date,
        userIsAdmin: Bool,
        avatarId: String?) {

        // Username cannot be empty
        guard !username.isEmpty else {
            print("Bad username")
            return nil
        }
        self.username = username

        // Email cannot be empty
        guard !email.isEmpty else {
            print("Bad email")
            return nil
        }
        self.email = email

        // joinDate cannot be empty
        guard joinDate == joinDate as Date else {
            print("Bad joinDate")
            return nil
        }
        self.joinDate = joinDate

        // lastVisited cannot be empty
        guard lastVisited == lastVisited as Date else {
            print("Bad lastVisited")
            return nil
        }
        self.lastVisited = lastVisited

        self.isAdmin = userIsAdmin

        self.avatarId = avatarId ?? nil
    }
}
