//
//  Shortcode.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class Shortcode {
    // MARK: Properties
    var shortcode: String
    var url: String
    var title: String
    var description: String?
    //var image_id: String?
    //var o_image_id: Bool?
    var isPublic: Bool
    var user: String
    var rating: Int
    var totalRatings: Int
    var views: Int
    var createdAt: Date
    var tags: String

    // MARK: Initialization

    init?(shortcode: String,
          url: String,
          title: String,
          description: String?,
          isPublic: Bool,
          user: String,
          rating: Int,
          totalRatings: Int,
          views: Int,
          createdAt: Date,
          tags: String) {

        // Shortcode cannot be empty
        guard !shortcode.isEmpty else {
            print("bad shortcode")
            return nil
        }
        self.shortcode = shortcode

        // URL cannot be empty
        guard !url.isEmpty else {
            print("Bad url")
            return nil
        }
        self.url = url

        // Title cannot be empty
        guard !title.isEmpty else {
            print("bad title")
            return nil
        }
        self.title = title

        self.description = description
        self.isPublic = isPublic

        // User cannot be empty
        guard !user.isEmpty else {
            print("bad user")
            return nil
        }
        self.user = user

        // Rating must be between 0 and 5
        guard rating >= 0 && rating <= 5 else {
            return nil
        }
        self.rating = rating

        self.totalRatings = totalRatings
        self.views = views
        self.createdAt = createdAt
        self.tags = tags

    }
}
