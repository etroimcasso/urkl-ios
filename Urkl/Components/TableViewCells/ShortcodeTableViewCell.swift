//
//  ShortcodeTableViewCell.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/18/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class ShortcodeTableViewCell: UITableViewCell, UITextViewDelegate {
    // MARK: Properties
    var shortcode: String = ""
    var url: String = ""
    var user: String = ""
    var title: String = ""
    var desc: String = ""
    var isPublic: Bool = true

    // MARK: Cell Colors
    var privateColor: UIColor = UIColor.orange.withAlphaComponent(0.5)
    var publicColor: UIColor = UIColor.white
    var privateTextColor: UIColor = UIColor.white
    var publicTextColor: UIColor = UIColor.black
    var privateLightTextColor: UIColor = UIColor.black
    var publicLightTextColor: UIColor = UIColor.darkGray

    // MARK: Outlets
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var viewsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: Actions
    @IBAction func titleButtonDidTap(_ sender: UIButton) {
        let urlString = UrklConfigurationManager.shared.baseUrl + "/" + shortcode
        //print(urlString)
        if let url = URL(string: urlString ) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil )

            //Need an end index that is the length of the string - 7, so suffix offset = length - 7
            let offset = -(viewsLabel.text!.count - "Views: ".count)
            let index = viewsLabel.text!.index(viewsLabel.text!.endIndex, offsetBy: offset)
            let views = "Views: \(Int(viewsLabel.text!.suffix(from: index))! + 1)"
            viewsLabel.text = views
        }
    }

    /**
 
    */
    func setCellColors(isPrivate: Bool) {
        self.isPublic = !isPrivate

        self.backgroundColor = (isPrivate) ? privateColor : publicColor
        urlLabel.textColor = (isPrivate) ? privateLightTextColor : publicLightTextColor
        titleButton.setTitleColor(
            (isPrivate) ? privateTextColor : publicTextColor,
            for: .normal)
        userLabel.textColor = (isPrivate) ? privateTextColor : publicTextColor
        descriptionField.textColor = (isPrivate) ? privateTextColor : publicTextColor
        viewsLabel.textColor = (isPrivate) ? privateTextColor : publicTextColor
        
        // Set description field's background 100% transparent
        descriptionField.backgroundColor = UIColor.clear
    }

}
