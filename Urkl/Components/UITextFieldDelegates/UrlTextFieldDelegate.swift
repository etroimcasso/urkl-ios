//
//  UrlTextFieldDelegate.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/20/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class UrlTextFieldDelegate: NSObject, UITextFieldDelegate {
    // MARK: Properties

    // MARK: UITextFieldDelegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
