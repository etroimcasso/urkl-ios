//
//  TitleTextFieldDelegate.swift
//  Urkl
//
//  Created by Eric Tomasso on 5/20/18.
//  Copyright © 2018 Etroimcasso. All rights reserved.
//

import UIKit

class TitleTextFieldDelegate: NSObject, UITextFieldDelegate {
    // MARK: Properties

    // MARK: UITextFieldDelegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }

    /**
    Enforce title length
    */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = UrklConfigurationManager.shared.maximumTitleLength
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
